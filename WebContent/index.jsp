<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Osmena Times</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<body>
	<div class="container">		
		<div class="row">
		
			<div class="col-md-3">
				<div class="list-group">
					<a class="list-group-item" class="active" href="#">Manage </a>
					<a class="list-group-item" href="${pageContext.request.contextPath}/create">Add new</a>
				</div>

				<div class="list-group">
					<a class="list-group-item" href="${pageContext.request.contextPath}/logout">Logout</a>
				</div>
			</div> <!-- ./col-md-6 -->
			
			<div class="col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">
						<c:if test="${not empty success}">
							<div class="alert alert-success">
								${success}
							</div>
						</c:if>

						<table class="table table-striped">
							<thead>
								<th>
									<td> # </td>
									<td> Controls </td>
									<td> Title </td>
								</th>
							</thead>

							<tbody>
								<c:forEach items="${requestScope.news}" var="article">
									<tr>
										<td> ${article.id} </td>
										<td> Hihi </td>
										<td> ${article.title} </td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>					
				</div>
			</div> <!-- ./col-md-6 -->
			
		</div> <!-- ./row -->
	</div> <!-- ./container -->
</body>
</html>
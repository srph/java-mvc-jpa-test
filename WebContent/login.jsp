<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Osmena Times - Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" type="text/css" rel="stylesheet">
</head>
<body>
	<div class="container">	
		<div class="panel panel-default" style="margin: auto; position: absolute; left: 0; right: 0; top: 0; bottom: 0;">
			<div class="panel-heading"> Login </div>
			<form class="panel-body" method="POST" action="${pageContext.request.contextPath}/login">
				<c:if test="${error != null}">
					<div class="alert alert-danger">
						<p> ${error} </p>
					</div>
				</c:if>

				<c:if test="${!empty success}">
					<div class="alert alert-success">
						<p> ${success} </p>
					</div>
				</c:if>
				
				<div class="form-group">
					<label> Username </label>
					<input type="text" class="form-control" name="username">
				</div>

				<div class="form-group">
					<label> Password </label>
					<input type="password" class="form-control" name="password">
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary">Login</button>
				</div>
			</form>
		</div>
	</div> <!-- ./container -->
</body>
</html>
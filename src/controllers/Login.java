package controllers;

import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.UserDAO;

@WebServlet(name = "login", urlPatterns = {"/login"})
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB private UserDAO userDAO;

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
	public void login(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		List user = userDAO.findFromAccountDetails(username, password);

		if(user != null)
		{
			request.getSession(false).setAttribute("user", user);

			String success = "You have been logged in successfully";
			request.setAttribute("success", success);

			response.sendRedirect("index.jsp");
		}
		else
		{
			String error = "Invalid username/password.";
			request.setAttribute("error", error);
			
			this.showForm(request, response);
		}
	}

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
	public void showForm(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		request
			.getRequestDispatcher("login.jsp")
			.forward(request, response);
	}
	
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		this.showForm(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException
	{
		this.login(request, response);
	}

}
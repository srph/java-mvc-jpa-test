package controllers;

import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.NewsDAO;
import models.News;

/**
 *
 * @author bagnes
 */
@WebServlet(name = "test", urlPatterns = {"/test"})
public class Test extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @EJB private NewsDAO newsDAO;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        List<News> news = newsDAO.all();

        // request.setAttribute("news", news);

        if(news == null) {
            System.out.println("Oops");
        } else {
            // System.out.println(news.toString());
            System.out.println("Naisu!");
        }
        // for(int i = 0; i < 5; i++)
        // {
        //     System.out.println(news[i].getTitle());
        // }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

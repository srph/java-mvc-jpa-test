package dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import models.News;

/**
 *
 * @author bagnes
 */
@Stateless
@LocalBean
public class NewsDAO {

    @PersistenceContext private EntityManager entity;
    
    public void add(News news)
    {
        entity.persist(news);
    }

    public void edit(News news)
    {
        entity.merge(news);
    }

    public void delete(String id)
    {
        News article = this.find(id);
        entity.remove(article);
    }

    public News find(String id)
    {
        return entity.find(News.class, id);
    }

    public List<News> all()
    {
        return entity.createNamedQuery("News.getAll").getResultList();
    }

}
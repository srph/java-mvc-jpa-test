package dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import models.User;

/**
 *
 * @author bagnes
 */
@Stateless
@LocalBean
public class UserDAO {

    // @Resource(name="jdbc/osmena_times") private DataSource ds;

    @PersistenceContext private EntityManager entity;
    
    public void add(User user)
    {
        entity.persist(user);
    }

    public void edit(User user)
    {
        entity.merge(user);
    }

    public void delete(String id)
    {
        User user = this.find(id);
        entity.remove(user);
    }

    public User find(String id)
    {
        return entity.find(User.class, id);
    }

    public List findFromAccountDetails(String username, String password)
    {
        return entity
            .createNamedQuery("User.getFromUsernameAndPassword")
            .setParameter("username", username)
            .setParameter("password", password)
            .getResultList();
    }

    public List all()
    {
        return entity.createNamedQuery("User.getAll").getResultList();
    }

}
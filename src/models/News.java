/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.*;
/**
 *
 * @author bagnes
 */
@Entity
@Table
@NamedQueries({@NamedQuery(name="News.getAll", query="SELECT e FROM news e")})
public class News {
    @Id
    @Column
    private String id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;

    public String getContent()
    {
        return content;
    }

    public News setContent(String content)
    {
        this.content = content;

        return this;
    }

    public String getTitle()
    {
        return title;
    }

    public News setTitle(String title)
    {
        this.title = title;
        return this;
    }

    public String getID()
    {
        return this.id;
    }

    public News setID(String id)
    {
        this.id = id;
        return this;
    }

    public News(String id, String title, String content)
    {
        this.id = id;
        this.title = title;
        this.content = content;
    }
}

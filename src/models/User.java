/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.*;
/**
 *
 * @author bagnes
 */
@Entity
@Table
@NamedQueries({
    @NamedQuery(name="User.getAll", query="SELECT e FROM users e"),
    @NamedQuery(name="User.getFromUsernameAndPassword", query="SELECT e FROM users WHERE username = :username WHERE password = :password LIMIT = 1")
})
public class User {
    @Id
    @Column
    private String id;
    @Column
    private String username;
    @Column
    private String password;

    public String getPassword()
    {
        return password;
    }

    public User setPassword(String password)
    {
        this.password = password;

        return this;
    }

    public String getUsername()
    {
        return username;
    }

    public User setUsername(String username)
    {
        this.username = username;
        return this;
    }

    public String getID()
    {
        return this.id;
    }

    public User setID(String id)
    {
        this.id = id;
        return this;
    }

    public User(String id, String username, String password)
    {
        this.id = id;
        this.username = username;
        this.password = password;
    }
}
